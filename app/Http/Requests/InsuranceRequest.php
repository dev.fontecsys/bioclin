<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsuranceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>"required|min:2",
            'acronym' =>"nullable|min:2",
        ];
    }

    public function messages()
    {
        return [
            'name.required' =>"Le nom de l'assurance est requise et doit avoir 2 lettre minimun",
            'acronym.required' =>"L'acronyme doit avoir 2 lettre minimum",
        ];
    }
}
