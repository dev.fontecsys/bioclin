<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\PrelevementURetralResult::class, function (Faker $faker) {
    return [
        'antibiogramme_id' => factory(App\Antibiogramme::class),
    ];
});
