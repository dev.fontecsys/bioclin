<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Physician::class, function (Faker $faker) {
    return [
        'lastname' => $faker->lastName,
        'qualification' => $faker->word,
        'firstname' => $faker->firstName,
        'order_number' => $faker->word,
        'hospital_id' => factory(App\Hospital::class),
    ];
});
