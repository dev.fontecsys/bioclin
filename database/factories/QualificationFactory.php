<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Qualification::class, function (Faker $faker) {
    return [
        'label' => $faker->word,
    ];
});
