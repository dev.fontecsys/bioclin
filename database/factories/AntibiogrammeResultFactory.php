<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\AntibiogrammeResult::class, function (Faker $faker) {
    return [
        'antibiotique_id' => $faker->randomNumber(),
        'antibiogramme_id' => $faker->randomNumber(),
        'result' => $faker->randomElement(['S', 'I', 'R']),
    ];
});
