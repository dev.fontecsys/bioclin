<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\PrelevementCervicoVaginal::class, function (Faker $faker) {
    return [
        'aspect_macroscopique' => $faker->randomElement(['Pateux', 'Laiteux', 'Glaireux', 'Autre']),
        'aspect_macroscopique_autre' => $faker->text,
        'couleur_macroscopique' => $faker->randomElement(['Chocolaté', 'Blanchâtre', 'Autre']),
        'couleur_macroscopique_autre' => $faker->text,
        'ph_macroscopique' => $faker->word,
        'test_potasse_macroscopique' => $faker->randomElement(['Positif', 'Negatif']),
        'leucocytes_microscopique' => $faker->text,
        'hematies_microscopique' => $faker->text,
        'cellules_microscopique' => $faker->text,
        'levures_microscopique' => $faker->text,
        'filaments_myceliens_microscopique' => $faker->text,
        'trichomonas_vaginalismicroscopique' => $faker->text,
        'microscopique_autre' => $faker->text,
        'coloration_gram' => $faker->randomElement(['Diplocoque Gram négatif', 'Bacille Gram négatif', 'Cocci Gram positifen amas', 'Absence de germes', 'Cocci Gram positif en chaiette', 'Mobilincus', 'Cocco Bacille Gram variable', 'CLUE CELLS']),
        'coloration_bacille_doderlein' => $faker->word,
        'coloration_type_flore' => $faker->word,
        'coloration_gram_apres_culture' => $faker->text,
        'identificatio_bacteriologie_apres_culture' => $faker->text,
        'identificatio_mycologie_apres_culture' => $faker->text,
        'antibiogramme_apres_culture' => $faker->randomElement(['Effectué', 'Non effectué']),
        'antifongigramme_apres_culture' => $faker->randomElement(['Effectué', 'Non effectué']),
        'conclusion_apres_culutre' => $faker->text,
        'antibiogramme_id' => factory(App\Antibiogramme::class),
    ];
});
