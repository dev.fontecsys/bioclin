<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\PnResult::class, function (Faker $faker) {
    return [
        'value' => $faker->word,
    ];
});
