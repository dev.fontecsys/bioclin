<?php

namespace App\Http\Controllers;

use App\Patient;
use App\Insurance;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\PatientRequest;
use Symfony\Component\HttpFoundation\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10;
        return   Patient::with(["demands","insurances"])->orderBy("created_at",'desc')->paginate($per);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientRequest $request)
    {
        //on commence la transaction
        DB::beginTransaction();
            try{
               $patient = Patient::create([
                'lastname' => $request->input('lastname'),
                'firstname' => $request->input('firstname'),
                'birthdate' => $request->input('birthdate'),
                'phone_number' => $request->input('phone_number'),
                'gender' => $request->input('gender'),
            ]);

            if($request->has('insurances'))
            {
                foreach($request->insurances as $insurance)
                {
                    $extra = 
                    [
                        "insurance_nr"=>$insurance['insurance_number'],
                        "main_insured"=>$insurance['main_insured'],
                        "main_insured_nr"=>$insurance['main_insured_number'],
                        "main_insured_name"=>$insurance['main_insured_name']
                    ];
                    $patient->insurances()->attach($insurance['insurance_id'],$extra);
                    Log::info($extra);
                }
            }
            return response()->json(['success' => true,'patient'=>$patient],200);

        }Catch(\Exception $e)
        {

            DB::rollback();
            Log::debug($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Patient::whereId($id)->with(["demands","insurances"])->orderBy("created_at",'desc')->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatientRequest $request, Patient $patient)
    {
         //on commence la transaction
             DB::beginTransaction();
                try{

                    $patient->lastname=$request->input("lastname");
                    $patient->firstname=$request->input("firstname");
                    $patient->birthdate=$request->input("birthdate");
                    $patient->phone_number=$request->input("phone_number");
                    $patient->gender=$request->input("gender");

                    $patient->load(['insurances']);

                    $patient->save();
                    

                    if(count($request->input('insurances'))>0)
                    {
                        Log::info("okdd");
                        foreach($request->insurances as $insurance)
                        {
                            $extra = 
                            [
                                "insurance_nr"=>$insurance['insurance_number'],
                                "main_insured"=>$insurance['main_insured'],
                                "main_insured_nr"=>$insurance['main_insured_number'],
                                "main_insured_name"=>$insurance['main_insured_name']
                            ];
                            $patient->insurances()->sync($insurance['insurance_id'],$extra);
                        }
                    }
                    else
                    {
                        Log::info("ok");
                        if($patient->fresh()->has('insurances'))
                        {

                            //On vide toutes les assurances
                            $patient->insurances()->detach();

                        }

                    }

            DB::commit();
            return response()->json(['success' => true,'patient'=>$patient->fresh()],200);

            }Catch(\Exception $e){
                DB::rollback();
                Log::debug($e->getMessage());
                return response()->json(['success' => false,"message"=>$e->getMessage()],201);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Patient::whereId($id)->delete();
           return response()->json(['success' => true,],200);
        } catch (\Exception $e) {
            DB::rollback();
            Log::debug($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }

    }
}
