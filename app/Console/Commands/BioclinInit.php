<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class BioclinInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bioclin:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init the app (migrations,seeding and passport)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //exec('composer install');
        Artisan::call('migrate:fresh');
        $this->info('All migrations has been refreshed');
        Artisan::call('db:seed');
        $this->info('Data has been seeded');
        Artisan::call('passport:install --force');
        $this->info('Pasport key has been generated');
        Artisan::call('php artisan key:generate');
        $this->info("Application key set successfully.");
        Artisan::call('cache:clear');
        $this->info('Cache has been cleared');
        Artisan::call('cache:cache');
        $this->info('Cache has been cached');
    }
}
