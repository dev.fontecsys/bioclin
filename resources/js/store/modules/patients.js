const state  =
{
    paginator:{},
    items:[],
    patient:null,
    patientAsParameter:[],
    loading:false,
    loaded:false,
    initialized:false,
    currentPage:0
};

const getters  = {
  initialized: state => {
    return state.initialized
  },
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    paginator: state => {
        return state.paginator
    },
    total: state => {
        return state.paginator.total
    },
    patient: state => {
      return state.patient
  },
};

const actions =
{
  async getPatient({commit},payload)
  {
     commit('SET_LOADING',true)

     var result = await axios.get('/api/patients/'+payload).then( response =>
          {
            commit('SET_PATIENT',response.data)
            commit('SET_LOADING',false)
          })
      return result;
  },
  
   async getPatients({commit},payload={page:""})
    {
       commit('SET_LOADING',true)
       var result = await axios.get(payload.page=="" ? '/api/patients/' :'/api/patients'+payload.query).then( response =>
            {
              commit('SET_PATIENTS',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async updatePatient({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/patients/'+payload.id,payload).then( response =>
            {

              commit('UPDATE_PATIENT',response.data.patient)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async addPatient({commit},payload)
    {

       commit('SET_LOADING',true)

       const result = await axios.post('/api/patients',payload)
       commit('ADD_PATIENT',result.data.patient)
       commit('SET_LOADING',false)
      
       return await result

    },
    async removePatient({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/patients/'+payload.id).then( response =>
            {
              commit('REMOVE_PATIENT',payload)
              commit('SET_LOADING',false)
            })
      return result;
    },

    init({commit})
    {
            commit('SET_PATIENTS',{})
            commit('SET_ITEMS',[])
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_PATIENTS(state,payload)
    {
        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []

            state.loaded = true
            if(state.initialized==false)
            {
             //state.initialized=true
            }
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
        //    alert(payload);
           state.paginator.data.forEach(element => {
            state.items.push(element)
          });
          state.loaded = true
          if(state.initialized==false)
          {
           //state.initialized=true
          }
        }


    },
    UPDATE_PATIENT(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_PATIENT(state,payload)
    {
            state.items.unshift(payload)
    },
    REMOVE_PATIENT(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },


    SET_ITEMS(state,payload)
    {
        state.items = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    },
    SET_PATIENT(state,payload)
    {
        state.patient = payload
    },
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
