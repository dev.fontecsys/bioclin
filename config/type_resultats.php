<?php 

return
[
    ["alpha_numeric_result","Résultat Alpha-numérique"],//1
    ["hemoculture","Hémoculture"],//2
    ["numeric_result","Résultat numérique"],//3
    ["prelevement_uretral_result","Prélevement urétral"],//4
    ["pn_result","Résultat positif-négatif"],//5
    ["prelevement_cervico_vaginal","Prélèvement cervico-vaginal"],//6
    ["recherche_mycosplasme","Recherche mycosplasme"],//7
    ["spermogramme","Spermogramme"],//8
    ["srv_result","SRV"],//9
    ["widal_felix_result","Widal et Felix"],//10


];