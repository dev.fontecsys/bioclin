<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class ResultTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = Config::get('type_resultats');
        
        foreach($types as $t){

            factory("App\ResultType",1)->create(['label'=> $t[1],'code'=>$t[0]]);
        }
    }
}
