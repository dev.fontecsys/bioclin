<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            //Ajout des roles
            $admin = factory(App\Role::class)->create(['name'=>"admin"]);
            $laborantin = factory(App\Role::class)->create(['name'=>"laborantin"]); 
            $os = factory(App\Role::class)->create(['name'=>"os_medical"]);  
            $rh = factory(App\Role::class)->create(['name'=>"os_medical"]);
            $financier = factory(App\Role::class)->create(['name'=>"financier"]);

            factory(App\User::class)->create(['name'=>"Mr Admin","email"=>"admin@bioclin.com",'role_id'=>$admin->id]);
            factory(App\User::class)->create(['name'=>"Minsta Raîssa","email"=>"rh@bioclin.com",'role_id'=>$rh->id]); 
            factory(App\User::class)->create(['name'=>"Moutsinga Bill","email"=>"financier@bioclin.com",'role_id'=>$financier->id]); 
            factory(App\User::class)->create(['name'=>"Ovono patrick","email"=>"laborantin1@bioclin.com",'role_id'=>$laborantin->id]); 
            factory(App\User::class)->create(['name'=>"Ondias Serges","email"=>"laborantin2@bioclin.com",'role_id'=>$laborantin->id]);
            factory(App\User::class)->create(['name'=>"Makelé Nicole","email"=>"os@bioclin.com",'role_id'=>$os->id]);

    }
}
