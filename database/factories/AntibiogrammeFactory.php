<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Antibiogramme::class, function (Faker $faker) {
    return [
        'germe' => $faker->word,
        'methode' => $faker->randomElement(['Diffusion sur gélose', 'ATB UR EU', 'ATB G-EU', 'ATB STAPH', 'VITEK 2']),
        'methode_autre' => $faker->word,
    ];
});
