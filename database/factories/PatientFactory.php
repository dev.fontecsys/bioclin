<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Patient::class, function (Faker $faker) {
    return [
        'lastname' => $faker->lastName,
        'firstname' => $faker->firstName,
        'birthdate' => $faker->date(),
        'phone_number' => $faker->phoneNumber,
        'gender' => $faker->randomElement(['f', 'm']),
        'user_id' => factory(App\User::class),
    ];
});
