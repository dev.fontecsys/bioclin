
const state  =
{
    paginator:{},
    initialized:false,
    items:[],
    POSITIF_NEGATIF:null,
    loading:false,
    loaded:false,
    POSITIF_NEGATIFAsParameter:[],
    currentPage:0
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      initialized: state => {
        return state.initialized
      },
      POSITIF_NEGATIFAsParameter: state => {
        return state.POSITIF_NEGATIFAsParameter
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    POSITIF_NEGATIF: state => {
      return state.POSITIF_NEGATIF
  },
};

const actions =
{
   async getPositifNegatifs({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

       await axios.get(payload.page=="" ? '/api/positif-negatif-result/' :'/api/positif-negatif-result'+payload.query).then( response =>
            {
              console.log("Promise Rejected:")

              commit('SET_POSITIF_NEGATIFS',response.data)
              commit('SET_LOADING',false)
            })
    },
    async updatePositifNegatif({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/positif-negatif-result/'+payload.id,payload).then( response =>
            {

              commit('UPDATE_POSITIF_NEGATIF',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async addPositifNegatif({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.post('/api/positif-negatif-result',payload).then( response =>
            {
              commit('ADD_POSITIF_NEGATIF',response.data.POSITIF_NEGATIF)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async getPOSITIF_NEGATIF({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.get('/api/positif-negatif-result/'+payload).then( response =>
            {
              commit('SET_POSITIF_NEGATIF',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async positifNegatifAsParameter({commit},payload)
    {
      // commit('SET_LOADING',true)

       var result = await axios.get('/api/membres/positifNegatifAsParameter?query='+payload).then( response =>
            {
              var data = Array.isArray(response.data.data) ? response.data.data : new Array()
              console.log(data)
              commit('POSITIF_NEGATIF_AS_PARAMETER',data)
              //commit('SET_LOADING',false)
            })
        return result;
    },
    async removePositifNegatif({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/membres/'+payload.id).then( response =>
            {
              commit('REMOVE_POSITIF_NEGATIF',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },

    init({commit})
    {
            commit('SET_POSITIF_NEGATIF',{})
            commit('SET_POSITIF_NEGATIFS',{})
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_POSITIF_NEGATIFS(state,payload)
    {


        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []
            state.loaded = true
            if(state.initialized==false)
            {
            // state.initialized=true
            }
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
           state.paginator.data.forEach(element => {
             state.items.push(element)
           });
           state.loaded = true
           if(state.initialized==false)
           {
            //state.initialized=true
           }

        }


    },
    UPDATE_POSITIF_NEGATIF(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_POSITIF_NEGATIF(state,payload)
    {
          state.items.unshift(payload)
    },
    POSITIF_NEGATIF_AS_PARAMETER(state,payload)
    {
          console.log("paykoad"+payload)
          state.POSITIF_NEGATIFAsParameter = payload ? payload : new Array()
    },
    REMOVE_POSITIF_NEGATIF(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },

    SET_POSITIF_NEGATIF(state,payload)
    {
        state.POSITIF_NEGATIF = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    }
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
