<?php

namespace App\Http\Controllers;

use App\Hospital;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HospitalController extends Controller
{
    
    public function index()
    {
        return view('layouts.master');
    }

    
    public function create()
    {
        
    }

   
    public function store(HospitalRequest $request)
    {
        try
        {
            DB::beginTransaction();

            $hospital = Hospital::create(
            [
                "name"=>$request->input('name'),
                'acronym' =>$request->input('acronym'),
                'city' =>$request->input('city'),
            ]);

            DB::commit();
        }
        catch(\Exception $e)
        {
                DB::rollback();
                return ['status'=>false,'message'=>$e->getMessage()];

        }
    }

    
    public function show($id)
    {
        
    }

    
    public function edit($id)
    {
        
    }

    
    public function update(HospitalRequest $request, Hospital $hospital)
    {
        try
        {
            DB::beginTransaction();

            $hospital->name = $request->input("name");
            $hospital->acronym = $request->input("acronym");
            $hospital->city = $request->input("city");
            $hospital->save();

            DB::commit();
        }
        catch(\Exception $e)
        {
                DB::rollback();
                return ['status'=>false,'message'=>$e->getMessage()];

        }
    }

    
    public function destroy($id)
    {
        
    }
}
