<?php

namespace App;

class Demand extends \App\AbstractModels\AbstractDemand
{
    protected $appends = ['cost','insured'];

    public function getCostAttribute()
    {
        $cost = 0;
        foreach($this->tests as $test)
        {
            $cost +=  $test->pivot->price;
        }
        return $cost;
    }

    private function getExam($examen) : Exam
    {
        if($examen instanceof \App\Exam)
        {
           return $examen;
        }
        else
        {
           if($examen!= null)
           {
               $examen = Exam::whereId($examen)->first(); 
               if(!$examen)
                return null ;
           }
           else
           {
               return null;
           }
        }
        return $examen;
    }

    public function addExam($examen,int $insurance=null)
    {
        //on vérifie si l'examen est objet App\Exam
         $examen = $this->getExam($examen);

        //On charge les assurances et le type de resultat de l'examen
        $examen->load(['insurances',"resultType"]);
        //on verifie que l'exam est assurée
        if($examen->hasAnyInsurance())
        {
           //si oui,on verifié que l'assurance n'est pas nulle
           if($insurance)
           {

           }
           else
           {
               
           }

        }
        //sinon on fixe le prix de base
        else
        {
            $this->tests($examen->id,["price"=>$examen->base_price,"insurance_id"=>null]);
        }
    }

    

    public function getInsuredAttribute()
    {
        $cost = 0;
        $nbr_examen =count($this->tests);
        $nbr_examen_assure =0;

        foreach($this->tests as $test)
        {
            if($test->pivot->insured_id != null);
            {
                $nbr_examen_assure++;
            }
        }

        if($nbr_examen_assure==$nbr_examen && $nbr_examen >0 )
        {
            return "oui ({$nbr_examen_assure}/{$nbr_examen}) ";
        }
        elseif($nbr_examen_assure<$nbr_examen && $nbr_examen >0 )
        {
            return "partiellement ({$nbr_examen_assure}/{$nbr_examen}) ";
        }
        else
        {
            return "non ({$nbr_examen_assure}/{$nbr_examen}) ";
        }

    }
}