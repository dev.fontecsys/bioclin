<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\NumericResult::class, function (Faker $faker) {
    return [
        'value' => $faker->randomFloat(),
    ];
});
