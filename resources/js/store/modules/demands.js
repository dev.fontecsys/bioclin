
const state  = 
{
    paginator:{},
    initialized:false,
    items:[],
    demand:null,
    loading:false,
    loaded:false,
    demandAsParameter:[],
    currentPage:0
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      initialized: state => {
        return state.initialized
      },
      demandAsParameter: state => {
        return state.demandAsParameter
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    demand: state => {
      return state.demand
  },
};

const actions = 
{
   async getDemands({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

       await axios.get(payload.page=="" ? '/api/demandes/' :'/api/demandes'+payload.query).then( response => 
            {
              console.log("Promise Rejected:")

              commit('SET_DEMANDS',response.data)
              commit('SET_LOADING',false)
            })
    },
    async updateDemand({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/demandes/'+payload.id,payload).then( response => 
            {

              commit('UPDATE_DEMAND',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async addDemand({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.post('/api/demandes',payload).then( response => 
            {
              commit('ADD_DEMAND',response.data.demand)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async getDemand({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.get('/api/demandes/'+payload).then( response => 
            {
              commit('SET_DEMAND',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async demandAsParameter({commit},payload)
    {
      // commit('SET_LOADING',true)

       var result = await axios.get('/api/membres/demandAsParameter?query='+payload).then( response => 
            {
              var data = Array.isArray(response.data.data) ? response.data.data : new Array()
              console.log(data)
              commit('DEMAND_AS_PARAMETER',data)
              //commit('SET_LOADING',false)
            })
        return result;
    },
    async removeDemand({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/membres/'+payload.id).then( response => 
            {
              commit('REMOVE_DEMAND',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },
    
    init({commit})
    {
            commit('SET_DEMAND',{})
            commit('SET_DEMANDS',{})
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations = 
{
    SET_DEMANDS(state,payload)
    {
      

        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []
            state.loaded = true
            if(state.initialized==false)
            {
            // state.initialized=true
            }
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = [] 
           }
           state.paginator.data.forEach(element => {
             state.items.push(element)
           });
           state.loaded = true
           if(state.initialized==false)
           {
            //state.initialized=true
           }

        }
        

    },
    UPDATE_DEMAND(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_DEMAND(state,payload)
    {
          state.items.unshift(payload)
    },
    DEMAND_AS_PARAMETER(state,payload)
    {
          console.log("paykoad"+payload)
          state.demandAsParameter = payload ? payload : new Array()
    },
    REMOVE_DEMAND(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },

    SET_DEMAND(state,payload)
    {
        state.demand = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload 
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    }
}

export default 
{
    namespaced : true,
    state,getters,actions,mutations
}