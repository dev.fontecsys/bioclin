<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Demand;
use App\Patient;
use App\ExamType;
use App\Insurance;
use App\ResultType;
use App\Antibiotique;
use ResultTypeSeeder;
use App\Qualification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParamsProviderController extends Controller
{
    public function dashboard()
    {
        $nb_demandes = Demand::all()->count();
        $nb_exams = Exam::all()->count();
        $nb_patients = Patient::all()->count();
        $nb_patients_now = Exam::whereYear('created_at', Now('Y'))->count();
        $demandes_recentes_statut = Demand::with(['patient'])->withCount(['tests'])->where('status','Enregistré')->latest()->take(5)->get();
        $demand_progress = Demand::with(['patient'])->withCount(['tests'])->where('status','En Cours')->latest()->take(1)->get();
        $demand_progress = $demand_progress[0];

        return [
            "demand_count" => $nb_demandes,
            "exam_count" => $nb_exams,
            "patient_count" => $nb_patients,
            'patient_count_current_year' => $nb_patients_now,
            'last_demands' => $demandes_recentes_statut,
            'demand_progress' => $demand_progress
        ];
    }

    public function get(Request $request)
    {
        //options pour la page vehicule
            return
            [
                "insurances"=>Insurance::all(),
                "exams"=>Exam::with(['examtype','insurances'])->get(),
                "exam_types"=>ExamType::all(),
                "qualifications"=>Qualification::all(),
                "result_types"=>ResultType::orderBy('label', 'asc')->get(),
                "patients"=>Patient::orderBy('firstname', 'asc')->get(),
                "antibiotiques"=>Antibiotique::orderBy('name', 'asc')->get(),
                "normes"=>DB::select('select norme from exams'),
            ];
    }
}
