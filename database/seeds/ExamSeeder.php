<?php

use App\Exam;
use App\Insurance;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class ExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $examens = Config::get('examens');
        $bio = factory("App\ExamType")->create(['label'=> "biologique"]);
        $bac = factory("App\ExamType")->create(['label'=> "bactériologique"]);

        foreach($examens as $examen)
        {

            if(trim($examen['type'])=="BIOLOGIQUE")
            {
                $exam = factory("App\Exam")->create(['name'=> $examen["nom"], 'base_price' => $examen["prix"],'norme'=> $examen["norme"], 'result_type_id'=> $examen["result_type_id"], 'exam_type_id'=>$bio->id]);
                foreach($examen['assurances'] as $ex)
                {
                    $insurance = Insurance::whereName($ex['nom'])->first();
                    if($insurance)
                    {
                        $exam->insurances()->attach($insurance->id,
                        [
                         'base_price' => $examen["prix"],
                         'price' => $ex['cotation'],
                         'tm' => $ex['tm'],
                         'gap' => $ex['gap'],
                         'gap_tm' => $ex['gap_tm']
                        ]);
                    }
                 }


            }

            if(trim($examen['type'])=="BACTERIOLOGIQUE")
            {
                $exam = factory("App\Exam")->create(['name'=> $examen["nom"], 'base_price' => $examen["prix"],'norme'=> $examen["norme"],'result_type_id'=> $examen["result_type_id"],'exam_type_id'=>$bac->id]);
                foreach($examen['assurances'] as $ex)
                {
                    $insurance = Insurance::whereName($ex['nom'])->first();
                    if($insurance)
                    {
                        $exam->insurances()->attach($insurance->id,
                        [
                         'base_price' => $examen["prix"],
                         'price' => $ex['cotation'],
                         'tm' => $ex['tm'],
                         'gap' => $ex['gap'],
                         'gap_tm' => $ex['gap_tm']
                        ]);
                    }
                 }
            }
        }
    }
}
