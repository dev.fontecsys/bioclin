<?php

use Illuminate\Database\Seeder;

class InsuranceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insurances = ['Ascoma','CNAMGS'];

        foreach($insurances as $insurance){
            factory("App\Insurance",1)->create(['name'=> $insurance,'acronym'=> $insurance]);
        }
    }
}
