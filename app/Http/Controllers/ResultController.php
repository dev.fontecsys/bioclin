<?php

namespace App\Http\Controllers;

use App\Test;
use App\Demand;
use App\PnResult;
use App\NumericResult;
use App\AlphaNumericResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ResultController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function positifNegatif(Request $request)
    {
        //on commence la transaction
        DB::beginTransaction();
            try{
               $PnResult = PnResult::create([
                'value' => $request->input('value'),
            ]);

        //Mise à jour de la table Test
        $id = $PnResult->id;
        $resultat_type = "App\PnResult";

        $test = Test::findOrFail($request->input('test_id'));

        $test->resultat_id = $id;
        $test->resultat_type= $resultat_type;
        $test->save();

        return response()->json(['success' => true,'PnResult'=>$PnResult],200);
        }Catch(\Exception $e)
        {
            DB::rollback();
            Log::debug($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function alphanumeric(Request $request)
    {
        DB::beginTransaction();
        try{

           $AlphaNumericResult = AlphaNumericResult::create([
            'value' => $request->input('value')
        ]);

        //Mise à jour de la table Test
        $id = $AlphaNumericResult->id;
        $resultat_type = "App\AlphaNumericResult";

        $test = Test::findOrFail($request->input('test_id'));

        $test->resultat_id = $id;
        $test->resultat_type= $resultat_type;
        $test->save();

        return response()->json(['success' => true,'AlphaNumericResult'=>$AlphaNumericResult],200);
        }Catch(\Exception $e)
        {
            DB::rollback();
            Log::debug($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function numeric(Request $request)
    {

        DB::beginTransaction();
        try{
           $NumericResult = NumericResult::create([
            'value' => $request->input('value'),
            'unite' => $request->input('unite'),
        ]);

        //Mise à jour de la table Test
        $id = $NumericResult->id;
        $resultat_type = "App\NumericResult";

        $test = Test::findOrFail($request->input('test_id'));

        $test->resultat_id = $id;
        $test->resultat_type= $resultat_type;
        $test->save();

        return response()->json(['success' => true,'NumericResult'=>$NumericResult],200);
        }Catch(\Exception $e)
        {
            DB::rollback();
            Log::debug($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getResult($id)
    {
        $idDemand = Test::whereDemand_id($id)->first();
        return  NumericResult::with(['test','test.exam','test.demand.patient'])->orderBy("created_at",'desc')->get();
        // return  Demand::whereId($id)->with(['patient','qualification','tests','tests.examtype'])->orderBy("created_at",'desc')->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
