
const state  = 
{
    paginator:{},
    initialized:false,
    items:[],
    tests:[],
    examen:null,
    examenAsParameter:[],
    loading:false,
    loaded:false,
    currentPage:0
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      initialized: state => {
        return state.initialized
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    _tests: state => {
      return state.tests
  },
    total: state => {
        return state.paginator.total
    },
    examen: state => {
      return state.examen
  },
}; 

const actions = 
{
  async getExamen({commit},payload)
  {
     commit('SET_LOADING',true)

     var result = await axios.get('/api/examens/'+payload).then( response => 
          {
            commit('SET_EXAMEN',response.data)
            commit('SET_LOADING',false)
          })
      return result;
  },
   async getExamens({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

       var result = await axios.get(payload.page=="" ? '/api/examens/' :'/api/examens'+payload.query).then( response => 
            {
              commit('SET_EXAMENS',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async updateExamen({commit},payload)
    {
       commit('SET_LOADING',true)
       
       var result = await axios.patch('/api/examens/'+payload.id,payload).then( response => 
            {
              commit('UPDATE_EXAMEN',response.data.exam)
              commit('SET_LOADING',false)
            })
        return result; 
    },
    async addExamen({commit,dispatch},payload)
    {
 
       commit('SET_LOADING',true)

       const result = await axios.post('/api/examens',payload).then( response => 
            {
              commit('ADD_EXAMEN',response.data.examen)
              dispatch('getExamens')
              commit('SET_LOADING',false)

            }) 
    
        return await result
        
    },
    async removeExamen({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/examens/'+payload.id).then( response => 
          {
            console.log(payload.id)
            commit('REMOVE_EXAMEN',payload)
            commit('SET_LOADING',false)
          })
      return result;
    },
    async getTest({commit},payload)
    {
      commit('SET_LOADING',true)

      var result = await axios.get('/api/resultat-demand/'+payload).then( response => 
            {
              commit('SET_TESTS',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    
    init({commit})
    {
            commit('SET_TESTS',{})
            commit('SET_EXAMENS',{})
            commit('SET_ITEMS',[])
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations = 
{
    SET_EXAMENS(state,payload)
    {
        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []

            state.loaded = true
            if(state.initialized==false)
            {
            state.initialized=true
            }
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = [] 
           }
           
           state.paginator.data.forEach(element => {
            state.items.push(element)
          });

          if(state.initialized==false)
           {
            state.initialized=true
           }
          state.loaded = true

        }
        

    },
    
    UPDATE_EXAMEN(state,payload)
    {
      const index = state.items.findIndex(exam => exam.id === payload.id);
      
      if (index >= 0) 
      {
        Object.assign(state.items[index],payload)
      }
    },
    ADD_EXAMEN(state,payload)
    {
            state.items.unshift(payload)
    },
    REMOVE_EXAMEN(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },


    SET_ITEMS(state,payload)
    {
        state.items = payload
    },
    SET_TESTS(state,payload)
    {   
      state.tests = payload
    },

    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload 
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    },
    SET_EXAMEN(state,payload)
    {
        state.examen = payload
    },
}

export default 
{ 
    namespaced : true,
    state,getters,actions,mutations
}