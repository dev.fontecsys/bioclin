<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check if insurances 
        $hasAnyinsurance = count($this->input('insurances'))>0;
        return [
            'name' =>"required|min:2",
            'base_price' => "numeric|min:3",
            'exam_type_id' => "required|exists:exam_types,id",
            'result_type_id' => "required|exists:result_types,id",

            'insurances.*.id' =>  $hasAnyinsurance  ? "required|exists:insurances,id" : '', 
            'insurances.*.pivot.price' =>  $hasAnyinsurance  ? "required|numeric" : '', 
            'insurances.*.pivot.tm' =>  $hasAnyinsurance  ? "required|numeric" : '', 
            'insurances.*.pivot.gap' =>  $hasAnyinsurance  ? "required|numeric" : '', 
            'insurances.*.pivot.gap_tm' =>  $hasAnyinsurance  ? "required|numeric" : '', 

        ];
    }


    public function messages()
    {
        return [
            'result_type_id.required' =>"Le type de résultat",
            'result_type_id.exists' =>"ce type de résultat est inconnu",
            'exam_type_id.required' =>"Le type d'examen",
            'exam_type_id.exists' =>"ce type d'examen est inconnu",
            'name.required' =>"Le nom est requis",
            'name.min' =>"Le nom doit contenir au moins 2 caractères",
            'base_price.numeric' => "Le prix doit être une valeur numérique"
        ]; 
    }
}
