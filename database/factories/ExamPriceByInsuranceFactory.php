<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\ExamPriceByInsurance::class, function (Faker $faker) {
    return [
        'id' => $faker->randomNumber(),
        'exam_id' => $faker->randomNumber(),
        'insurance_id' => $faker->randomNumber(),
        'price' => $faker->randomFloat(),
        'tm' => $faker->randomFloat(),
        'gap_tm' => $faker->randomFloat(),
        'gap' => $faker->randomFloat(),
        'base_price' => $faker->randomFloat(),
    ];
});
