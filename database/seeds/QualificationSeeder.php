<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class QualificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qualifications = Config::get('qualifications');
        
        foreach($qualifications as $q){

            factory("App\Qualification",1)->create(['label'=> ucfirst($q)]);
        }
    }
}
