<?php

use App\ResultType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(UserSeeder::class);
        $this->call(ResultTypeSeeder::class);
        $this->call(InsuranceSeeder::class);
        $this->call(ExamSeeder::class); 
        $this->call(QualificationSeeder::class); 
        $this->call(PatientSeeder::class);   
        factory("App\Demand",25)->create([]); 
    
    }
}
