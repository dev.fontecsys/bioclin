
const state  =
{
    paginator:{},
    initialized:false,
    items:[],
    numeric:null,
    loading:false,
    loaded:false,
    numericAsParameter:[],
    currentPage:0
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      initialized: state => {
        return state.initialized
      },
      numericAsParameter: state => {
        return state.numericAsParameter
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    numeric: state => {
      return state.numeric
  },
};

const actions =
{
   async getNumerics({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

       await axios.get(payload.page=="" ? '/api/numeric-result/' :'/api/numeric-result'+payload.query).then( response =>
            {
              console.log("Promise Rejected:")

              commit('SET_NUMERICS',response.data)
              commit('SET_LOADING',false)
            })
    },
    async updateNumeric({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/numeric-result/'+payload.id,payload).then( response =>
            {

              commit('UPDATE_NUMERIC',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async addNumeric({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.post('/api/numeric-result',payload).then( response =>
            {
              commit('ADD_NUMERIC',response.data.numeric)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async getNumeric({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.get('/api/numeric-result/'+payload).then( response =>
            {
              commit('SET_NUMERIC',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async numericAsParameter({commit},payload)
    {
      // commit('SET_LOADING',true)

       var result = await axios.get('/api/membres/numericAsParameter?query='+payload).then( response =>
            {
              var data = Array.isArray(response.data.data) ? response.data.data : new Array()
              console.log(data)
              commit('NUMERIC_AS_PARAMETER',data)
              //commit('SET_LOADING',false)
            })
        return result;
    },
    async removeNumeric({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/membres/'+payload.id).then( response =>
            {
              commit('REMOVE_NUMERIC',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },

    init({commit})
    {
            commit('SET_NUMERIC',{})
            commit('SET_NUMERICS',{})
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_NUMERICS(state,payload)
    {


        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []
            state.loaded = true
            if(state.initialized==false)
            {
            // state.initialized=true
            }
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
           state.paginator.data.forEach(element => {
             state.items.push(element)
           });
           state.loaded = true
           if(state.initialized==false)
           {
            //state.initialized=true
           }

        }


    },
    UPDATE_NUMERIC(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_NUMERIC(state,payload)
    {
          state.items.unshift(payload)
    },
    NUMERIC_AS_PARAMETER(state,payload)
    {
          state.numericAsParameter = payload ? payload : new Array()
    },
    REMOVE_NUMERIC(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },

    SET_NUMERIC(state,payload)
    {
        state.numeric = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    }
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
