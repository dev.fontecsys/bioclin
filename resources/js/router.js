import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
//action
const guest = (to, from, next) => {
  if (!localStorage.getItem("token")) {
    return next();
  } else {
    return next("/");
  }
};
const auth = (to, from, next) => {
  if (localStorage.getItem("token")) {
    return next();
  } else {
    return next("/login");
  }
};
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'dashboard',
      beforeEnter: auth,
      component: require('./pages/Dashboard.vue').default

     // component: require('./../pages/Dashboard.vue').default
    },
    {
        path: '/examens',
        name: 'examens',
        beforeEnter: auth,
        component: require('./pages/examens/Examens.vue').default

       // component: require('./../pages/Dashboard.vue').default
      },
      {
        path: '/demandes',
        name: 'demandes',
        beforeEnter: auth,
        component: require('./pages/demandes/Demandes.vue').default

       // component: require('./../pages/Dashboard.vue').default
      },
      {
        path: '/demandes/:id',
        name: 'demand-detail',
        beforeEnter: auth,
        component: require('./pages/demandes/Demand_Detail.vue').default

       // component: require('./../pages/Dashboard.vue').default
      },
      {
        path: '/nouvelle-demande',
        name: 'nouvelle-demande',
        beforeEnter: auth,
        component: require('./pages/demandes/Demand_add.vue').default

       // component: require('./../pages/Dashboard.vue').default
      },
      {
        path: '/patients',
        name: 'patients',
        beforeEnter: auth,
        component: require('./pages/patients/Patients.vue').default

       // component: require('./../pages/Dashboard.vue').default
      },
      {
        path: '/patients/:id',
        name: 'patient_detail',
        beforeEnter: auth,
        component: require('./pages/patients/PatientDetail.vue').default

       // component: require('./../pages/Dashboard.vue').default
      },
      {
        path: '/laboratoires',
        name: 'laboratoires',
        component: require('./pages/Laboratoires.vue').default

       // component: require('./../pages/Dashboard.vue').default
      },
    { path: '/', redirect: '/dashboard' },
    {
        path: '/login',
        name: 'login',
        beforeEnter: guest,
        component: require('./components/LoginComponent.vue').default
    },
    {
      path: '/inscription',
      name: 'inscription',
      //beforeEnter: guest,
      component: require('./components/RegisterComponent.vue').default
  }
    //    // component: require('./../pages/Dashboard.vue').default
    // },
    // { path: '/404', name: '404', component: require('./pages/NotFound.vue').default },
    //  { path: '*', redirect: '/404' },
  ],
  linkActiveClass: 'active'
})
