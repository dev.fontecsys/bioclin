const  state = {
      errors:[],
      status: '',
      token: localStorage.getItem('token') || '',
      user : JSON.parse(localStorage.getItem('user')) || {}
    }
  
    const   getters = {
      errors: state => state.errors,
      isLoggedIn: state => !!state.token,
      _user: state => state.user,
      authStatus: state => state.status,
    }
  
  const  mutations = {

      setErrors(state, errors) {
        state.errors = errors;
      },
      auth_request(state){
        state.status = 'loading'
      },
      auth_success(state, {token, user}){
        state.status = 'success'
        state.token = token
        state.user = user
      },
      auth_error(state){
        state.status = 'error'
      },
      logout(state){
        state.status = ''
        state.token = ''
        state.user = {}
      },
    }
  
  const  actions = {
      //Action pour se connecter à l'application
      sendLoginRequest({commit}, user)
      {
          return new Promise((resolve, reject) => {
            commit('auth_request')
            axios({url: '/api/login', data: user, method: 'POST' })
            .then(resp => {
              const token = resp.data.token.token
              const user = resp.data.user

              localStorage.setItem('token', token)
              var stringifyUser  =  JSON.stringify(user)
              localStorage.setItem('user',""+stringifyUser+"")

              window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
              commit('auth_success',{token, user})
              resolve(resp)
            })
            .catch(err => {
              commit('auth_error')
              localStorage.removeItem('token')
              reject(err)
            })
          })
      },
      //
      register({commit}, user){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios({url: '/register', data: user, method: 'POST' })
        .then(resp => 
          {
          console.log(resp.data.token.token,resp.data.token)
          const token = resp.data.token.token
          const user = resp.data.user
          localStorage.setItem('token', token)
          window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
          commit('auth_success', token, user)
          resolve(resp)
        })
        .catch(err => {
          commit('auth_error', err)
          localStorage.removeItem('token')
          reject(err)
        })
      })
      },
      //Déconnexion
      logout({commit}){
      
      return new Promise((resolve, reject) => {
        commit('logout')
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        delete window.axios.defaults.headers.common['Authorization']
        resolve()
      })
      },
      sendVerifyResendRequest() {
        return axios.get(process.env.VUE_APP_API_URL + "email/resend");
      },
      sendVerifyRequest({ dispatch }, hash) {
        return axios
          .get(process.env.VUE_APP_API_URL + "email/verify/" + hash)
          .then(() => {
            dispatch("getuser");
          });
      }
    }
    
export default
{
    namespaced : true,
    state,getters,actions,mutations
}
