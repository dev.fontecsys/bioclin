<?php 

return
[
    ["nom"=>"ECBU","prix"=>"12500", "norme"=>"", "result_type_id"=>"4","type"=>"BACTERIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ],
        [
            ["nom"=>"ascoma","cotation"=>6000,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],
    ["nom"=>"P urétral","prix"=>"15000", "norme"=>"", "result_type_id"=>"4","type"=>"BACTERIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"PV","prix"=>"15000", "norme"=>"", "result_type_id"=>"6","type"=>"BACTERIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Mycoplasmes","prix"=>"20000", "norme"=>"", "result_type_id"=>"7","type"=>"BACTERIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    // ["nom"=>"8","prix"=>"15000", "norme"=>"", "result_type_id"=>"8","type"=>"BACTERIOLOGIQUE","assurances" => 
    //     [
    //         ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
    //     ]],
    ["nom"=>"Spermocytogramme","prix"=>"15000", "norme"=>"", "result_type_id"=>"8","type"=>"BACTERIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Spermoculture","prix"=>"20000", "norme"=>"", "result_type_id"=>"8","type"=>"BACTERIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]], 
    ["nom"=>"Coproculture","prix"=>"15000", "norme"=>"", "result_type_id"=>"2","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"pus","prix"=>"10000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Bandelettes urinaires","prix"=>"3000", "norme"=>"", "result_type_id"=>"4","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Hémoculture","prix"=>"8000", "norme"=>"", "result_type_id"=>"2","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Ballon","prix"=>"10000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"NFS ","prix"=>"9000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"TP/INR","prix"=>"3000", "norme"=>"(70-100)%", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"TCA","prix"=>"4000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"GE","prix"=>"4000", "norme"=>"", "result_type_id"=>"1","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Gs Rh","prix"=>"4000", "norme"=>"", "result_type_id"=>"1","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"VSH","prix"=>"2500", "norme"=>"", "result_type_id"=>"1","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],

    ["nom"=>"VS2H","prix"=>"2500", "norme"=>"", "result_type_id"=>"1","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],

    ["nom"=>"EL-Hb","prix"=>"12000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Urée","prix"=>"2500", "norme"=>"(0.5 - 7.5)mmol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Créatinine","prix"=>"2500", "norme"=>"(30 - 120)umol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Acide Urique","prix"=>"3000", "norme"=>"(150 - 420)umol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Glycémie à jeun","prix"=>"2500", "norme"=>"(3.9 - 5.9)umol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Glycémie post-prandiale","prix"=>"2500", "norme"=>"(8 - 9)mmol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Cholestérol total","prix"=>"3000", "norme"=>"(3.5 - 5)mmol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"HDL-Cholestérol ","prix"=>"4000", "norme"=>"(1 - 1.5)mmol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"LDL-Cholestérol","prix"=>"4000", "norme"=>"(1 - 2)mmol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],

    ["nom"=>"Ionogramme Sanguin Na+","prix"=>"14.000", "norme"=>"(130 - 145)mEq/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Spécial
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],
    ["nom"=>"Ionogramme Sanguin K+","prix"=>"14.000", "norme"=>"(3.00 - 5.5)mEq/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Spécial
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],
    ["nom"=>"Ionogramme Sanguin Cl-","prix"=>"14.000", "norme"=>"(98 - 110)mEq/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Spécial
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],
    ["nom"=>"Calcémie","prix"=>"3500", "norme"=>"(2 - 2.55)mmol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Phosphore ","prix"=>"3500", "norme"=>"(2 - 2.25)mmol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Magnésium","prix"=>"3500", "norme"=>"(0.99 - 1.05)mmol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],

    ["nom"=>"Transaminases GOP","prix"=>"7000", "norme"=>"≤ 40 Ui/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Spécial
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],
    ["nom"=>"Transaminases GPT","prix"=>"7000", "norme"=>"≤ 37 Ui/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Spécial
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],

    ["nom"=>"Gamma GT","prix"=>"7000", "norme"=>"(5 - 39)Ui/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"PAL","prix"=>"3500", "norme"=>"≤ 120 Ui/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Amylase","prix"=>"8500", "norme"=>"< 80 Ui/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Fer Sérique ","prix"=>"7000", "norme"=>"Ad: (6.6 -8.)umol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"ALSO","prix"=>"6000", "norme"=>"", "result_type_id"=>"5","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Lipase ","prix"=>"12000", "norme"=>"< 60 Ui/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"LDH","prix"=>"14000", "norme"=>"[Enf:(120-300)Ui/L,F:(135-214)Ui/L,H:(135-225)Ui/L]", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Protides totaux","prix"=>"2500", "norme"=>"(70 - 80)g/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Albuminémie","prix"=>"6000", "norme"=>"[Enf: (28-44) g/l, Ad: (34-488) g/l]", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"CRP","prix"=>"12000", "norme"=>"(0-6)mg/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Bilirubine Total","prix"=>"2500", "norme"=>"≤ 17 umol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Bilirubine Direct","prix"=>"2500", "norme"=>"(0-5) umol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Bilirubine Indirect","prix"=>"2500", "norme"=>"(12-17) umol/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Albumine/Sucre","prix"=>"2500", "norme"=>"", "result_type_id"=>"5","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"HbA1C","prix"=>"15000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Micro Albumine ","prix"=>"12000", "norme"=>"< 20 mg/l", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"BHCG Qualitatif","prix"=>"9000", "norme"=>"(0-5) Ui/L", "result_type_id"=>"5","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"BHCG Quantitatif","prix"=>"17000", "norme"=>"(0-5) Ui/L", "result_type_id"=>"1","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Prolactine","prix"=>"17000", "norme"=>"[F: (0-20) ng/mL,F.enc: (20-400) ng/mL]", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"FSH","prix"=>"17000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Non confirmé
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"LH","prix"=>"8000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Non confirmé
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Œstradiol  (Ell2)","prix"=>"20000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" =>//Non confirmé 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"TSH","prix"=>"17000", "norme"=>"(0.5-4.0) uUl/ml)", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"T3 ","prix"=>"14000", "norme"=>"< 3 ng/mL", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"FT3","prix"=>"17000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"T4 (Thyroxine)","prix"=>"14000", "norme"=>"(4-11) mg/dL", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"FT4","prix"=>"17000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Non confirmé 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Insuline","prix"=>"26000", "norme"=>"< 20 uUi/ml", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"ferritine","prix"=>"20000", "norme"=>"[H:(12-300)ng/mL,F:(10-150)ng/mL]", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"PSA Totale","prix"=>"20000", "norme"=>"[H<70ans<4ng/ml,H>70ans<6.5ng/ml]", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Non confirmé 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"PSA Libre","prix"=>"20000", "norme"=>"[H<70ans<4ng/ml,H>70ans<6.5ng/ml]", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => //Non confirmé 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"CA125","prix"=>"27000", "norme"=>"(0-335) Ui/ml", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"CA15-3","prix"=>"27000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"CA19-9","prix"=>"27000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"A FP","prix"=>"20000", "norme"=>"[Enf<1ans:(0-30) ng/ml,Ad<40 ng/ml]", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"ACE","prix"=>"20000", "norme"=>"(0-5) ng/mL", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Troponine ","prix"=>"20000", "norme"=>"< 0.8 ng/mL", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"D-Dimères","prix"=>"7500", "norme"=>"< 0.5 mg/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"CPK","prix"=>"8000", "norme"=>"< 80 U/L", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"CK-MB","prix"=>"9000", "norme"=>"< 10 ng/mL", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"BW (TPHA, VDRL)","prix"=>"9000", "norme"=>"", "result_type_id"=>"5","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Chlamydiae","prix"=>"17000", "norme"=>"", "result_type_id"=>"10","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Toxoplasmose IgG","prix"=>"17000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],
    ["nom"=>"Toxoplasmose IgM","prix"=>"17000", "norme"=>"", "result_type_id"=>"10","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]
    ],
    ["nom"=>"Rubéole IgG","prix"=>"20000", "norme"=>"", "result_type_id"=>"3","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"SRV","prix"=>"17000", "norme"=>"", "result_type_id"=>"9","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Widal et Felix","prix"=>"7000", "norme"=>"", "result_type_id"=>"10","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Ag Hbs","prix"=>"12000", "norme"=>"", "result_type_id"=>"5","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Hépatite A","prix"=>"20000", "norme"=>"", "result_type_id"=>"5","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"AC anti HCV","prix"=>"20000", "norme"=>"", "result_type_id"=>"5","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
    ["nom"=>"Sérologie Amibienne","prix"=>"9000", "norme"=>"", "result_type_id"=>"5","type"=>"BIOLOGIQUE","assurances" => 
        [
            ["nom"=>"cnamgs","cotation"=>8750,'tm'=>1750,'gap'=>3250,'gap_tm'=>5000,"sans_assurance"=>12500]
        ]],
];
