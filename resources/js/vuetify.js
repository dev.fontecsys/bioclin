import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

const opts = {
  breakpoint: {
    thresholds: {
      xs: 340,
      sm: 540,
      md: 800,
      lg: 1280,
    },
    scrollBarWidth: 24,
  },
    theme: {
      themes: {
        light: {
          primary: '#58aff0',
          secondary: '#b0bec5',
          accent: '#8c9eff',
          error: '#b71c1c',
        }
      }
    }
  }
export default new Vuetify(opts)