<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\ResultType::class, function (Faker $faker) {
    return [
        'label' => $faker->word."-".Str::random(5),
        'code' => $faker->word."-".Str::random(15)
    ];
});
