<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' =>"required|min:2",
            'firstname' =>"nullable|min:2",
            'birthdate' => "required|date",
            'phone_number' => "required",

            'insurances.*.insurance_id' => "required|exists:insurances,id",
            'insurances.*.insurance_number' => "required",
            'insurances.*.main_insured_number' => !$this->input('insurances.*.main_insured') ? "required" : "",
            'insurances.*.main_insured_name' => !$this->input('insurances.*.main_insured') ? "required" : "",

        ];
    }

    public function messages()
    {
        return [
            'lastname.required' =>"Le nom du patient est requis",
            'lastname.min' =>"Le nom du patient doit avoir au moins 2 caractères",
            'firstname.required' =>"Le prénom du chauffeur est requis",
            'birthdate.required' =>"La date de naissance du patient est requise",
            'phone_number.required' =>"Le numero de téléphone du chauffeur est requis",
        ];
    }
}
