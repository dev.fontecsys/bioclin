<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Insurance;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\Demand::class, function (Faker $faker) {
    $insurance = Insurance::whereName('cnamgs')->first();
    if($insurance)
    return [
        'file_number' => Carbon::now()->toDateString()."-".strtoupper(Str::random(4)),
        'name_physician' => "Dr ".$faker->firstName()." ".$faker->lastName(),
        'patient_id' => factory(App\Patient::class),
        'insurance_id' => $insurance->id,
        'status' => $faker->randomElement(['Enregistré', 'En cours', 'Traité', 'Annulé']),
        'order_number_physician' => $faker->randomNumber(),
        'hospital' => $faker->word,
        'qualification_id' => factory(App\Qualification::class),
        'user_id' => factory(App\User::class),
    ];
});
