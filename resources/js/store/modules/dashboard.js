
const state  =
{
    demand_count:0,
    exam_count:0,
    patient_count:0,
    demand_progress: {},
    patient_current_year_count :0,
    last_demands : []
};

const getters  = {

    _demand_count: state => {
        return state.demand_count
      },
      _exam_count: state => {
        return state.exam_count
      },

      _patient_count: state => {
        return state.patient_count
      },
      _patient_current_year_count: state => {
        return state.patient_current_year_count
      },
      _last_demands: state => {
        return state.last_demands
      },

      _demand_progress: state => {
        return state.demand_progress
      },
};

const actions =
{
    async callDashboard({commit})
    {

      await axios.get("/api/params/provider/dashboard").then((response) =>
      {



        commit('SET_EXAM_COUNT',response.data.exam_count)
        commit('SET_DEMAND_COUNT',response.data.demand_count)
        commit('SET_PATIENT_COUNT',response.data.patient_count)
        commit('SET_LAST_DEMANDS',response.data.last_demands)
        commit('SET_DEMAND_PROGRESS',response.data.demand_progress)
        commit('SET_PATIENT_CURRENT_YEAR_COUNT',response.data.patient_count_current_year)


      },
      (error) =>
      {
        console.log(error)
      });
    },
}

const mutations =
{
    SET_DEMAND_COUNT(state,payload)
    {
         state.demand_count = payload
    },
    SET_PATIENT_COUNT(state,payload)
    {
         state.patient_count = payload
    },
    SET_EXAM_COUNT(state,payload)
    {
       state.exam_count = payload
    },
    SET_LAST_DEMANDS(state,payload)
    {
       state.last_demands = payload
    },
    SET_DEMAND_PROGRESS(state,payload)
    {
       state.demand_progress = payload
    },
    SET_PATIENT_CURRENT_YEAR_COUNT(state,payload)
    {
       state.patient_current_year_count = payload
    }
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
