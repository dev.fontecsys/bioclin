<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Role;
use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {

    $role = Role::inRandomOrder()->first();
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'email_verified_at' => $faker->dateTime(),
        'password' => bcrypt("password"),
        'remember_token' => Str::random(10),
        'role_id' => $role ? $role->id :factory(App\Role::class),

    ];
});
