<?php

namespace App\Http\Controllers;

use App\Insurance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\InsuranceRequest;

class InsuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insurances = Insurance::all();
        return response()->json($insurances);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsuranceRequest $request)
    {
        //on commence la transaction
        DB::beginTransaction();
            try{
                Insurance::create([
                'name' => $request->input('name'),
                'acronym' => $request->input('acronym'),
            ]);

            return response()->json(['success' => true,],200);

        }Catch(\Exception $e){
            DB::rollback();
            Log::debug($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InsuranceRequest $request, Insurance $insurance)
    {
         //on commence la transaction
             DB::beginTransaction();
                try{

                    $insurance->name=$request->input("name");
                    $insurance->name=$request->input("name");

            DB::commit();
            return response()->json(['success' => true,],200);

            }Catch(\Exception $e){
                DB::rollback();
                Log::debug($e->getMessage());
                return response()->json(['success' => false,"message"=>$e->getMessage()],201);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Insurance $insurance)
    {
        try {
            $insurance->delete();
           return response()->json(['success' => true,],200);
        } catch (\Exception $e) {
            DB::rollback();
            Log::debug($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }
}
