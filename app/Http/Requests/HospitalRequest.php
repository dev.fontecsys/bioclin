<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HospitalRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'acronym' => 'nullable|min:2',
            'city' => 'nullable|min:2'
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'Le nom est requis',
            'name.min' => 'Le nom doit comporter au moins 2 caractères',
            'acronym.min' => "L'acronyme doit comporter au moins 2 caractères",
            'city.min' => 'La ville doit comporter au moins 2 caractères'
        ];
    }
}
