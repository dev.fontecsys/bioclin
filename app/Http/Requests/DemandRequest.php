<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DemandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            //Patient
            'patient.id'=>$this->input('patient_is_know')==true ?"required|exists:patients,id":'nullable',
            'patient.lastname' =>"required|min:2",
            'patient.firstname' =>"nullable|min:2",
            'patient.birthdate' =>"required|date",
            'patient.gender' =>"required|in:F,M,m,f",
            'patient.phone_number' =>"required",
            //medecin
            'name_physician' =>"required|min:2",
            'order_number_physician' => "nullable",
            'hospital' => "required",
            'qualification_id' => "required",
            //insurances
            'insurances.*.insurance_id'    => $this->input('patient_is_insured') ? "required|exists:insurances,id" : "nullable",
            'insurances.*.insurance_number'    => $this->input('patient_is_insured') ? "required": "nullable",
            'insurances.*.main_insured'=> $this->input('patient_is_insured') ?'required': "nullable",
            'insurances.*.main_insured_name'=>$this->input('patient_is_insured') && $this->input('insurances.*.main_insured')==false ?"required":'nullable',
            'insurances.*.main_insured_number'=>$this->input('patient_is_insured') && $this->input('insurances.*.main_insured')==false ?"required":'nullable',
            //examens
            'exams.*.id' => "required|exists:exams,id",
            'exams.*.insurance_id' => $this->input('patient_is_insured') ? "required|exists:insurances,id" :'nullable',
            'exams.*.name' => "required",
            'exams.*.payement_without_insurance'=>'required',
            'exams.*.base_price'=>'required',
            'exams.*.current_price'=>'required',

        ];
    }

        /**
     * Get the messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
