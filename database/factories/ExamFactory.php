<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\ExamType;
use App\ResultType;
use Faker\Generator as Faker;

$factory->define(App\Exam::class, function (Faker $faker) {
    $rt = ResultType::inRandomOrder()->first();
    $et = ExamType::inRandomOrder()->first();
    return [
        'name' => $faker->name,
        'base_price' => $faker->randomFloat(),
        'result_type_id' => $rt->id,
        'exam_type_id' => $et->id,
        'user_id' => factory(App\User::class),
    ];
});
