
const state  =
{
    pays:[],
    civilites:['m.','mme','mlle','dr','pr'],
    genders:[{label:'Masculin',value:"m"},{label:'Féminin',value:"f"}],
    statuts_maritaux:['célibataire','marié(e)','veuf(ve)','concubinage'],
    qualifications:[],
    insurances:[],
    exam_types:[],
    result_types:[],
    exams:[],
    unites:[{ name: 'ml' },{ name: 'mmol/L' },{ name: 'mEq/L' },{name: 'µmol/L' },{name: 'µUI/ml' },{name: 'mg/l' },{name: 'g/L' },{name: 'g/l' },{name: 'UI/L' },{name: 'UI/ml' },{name: 'U/L' },{name: '%' },{name: 's' },{name: 'ng/mL' },{name: 'mg/L' },{name: 'mg/dL' },{name: 'pg/ml' }],
    patients:[],
    antibiotiques:[]
};

const getters  = {

    _pays: state => {
        return state.pays
      },
      _patients: state => {
        return state.patients
      },
      _qualifications: state => {
        return state.qualifications
      },
      _genders: state => {
        return state.genders
      },
      _civilites: state => {
        return state.civilites
      },
      _statuts_maritaux: state => {
        return state.statuts_maritaux
      },
      _insurances: state => {
        return state.insurances
      },

      _exam_types: state => {
        return state.exam_types
      },
      _result_types: state => {
        return state.result_types
      },
      _exams: state => {
        return state.exams
      },
      _unites: state => {
        return state.unites
      },
      _antibiotiques: state => {
        return state.antibiotiques
      },
};

const actions =
{
    async loadOptions({commit})
    {

      await axios.get("/api/params/provider").then((response) =>
      {


        // commit('SET_PAYS',response.data.pays)
        // commit('SET_FORMATIONS',response.data.formations)
        // commit('SET_SECTEURS',response.data.secteurs)
        commit('SET_EXAMS',response.data.exams)
        commit('SET_QUALIFICATIONS',response.data.qualifications)
        commit('SET_INSURANCES',response.data.insurances)
        commit('SET_EXAM_TYPES',response.data.exam_types)
        commit('SET_RESULT_TYPES',response.data.result_types)
        commit('SET_PATIENTS',response.data.patients)
        commit('SET_ANTIBIOTIQUES',response.data.antibiotiques)

      },
      (error) =>
      {
        console.log(error)
      });
    },
}

const mutations =
{
    SET_PAYS(state,payload)
    {
         state.pays = payload
    },
    SET_QUALIFICATIONS(state,payload)
    {
         state.qualifications = payload
    },
    SET_INSURANCES(state,payload)
    {
       state.insurances = payload
    },
    SET_EXAM_TYPES(state,payload)
    {
       state.exam_types = payload
    },
    SET_RESULT_TYPES(state,payload)
    {
       state.result_types = payload
    }
    ,
    SET_EXAMS(state,payload)
    {
       state.exams = payload
    },
    SET_GENDERS(state,payload)
    {
       state.genders = payload
    },
    
    SET_PATIENTS(state,payload)
    {
         state.patients = payload
    },

    SET_ANTIBIOTIQUES(state,payload)
    {
         state.antibiotiques = payload
    },
    SET_NORMES(state,payload)
    {
       state.normes = payload
    },
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
