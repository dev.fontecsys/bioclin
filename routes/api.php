<?php

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'Auth\LoginController@login');

Route::middleware(['auth:api',"role"])->group(function() {
    Route::resource('patients', 'PatientController');
    Route::resource('demandes', 'DemandController');
    Route::resource('examens', 'ExamController');
    Route::resource('assurances', 'InsuranceController');
    Route::get('api/patient/{id}', 'PatientController@destroy');
    Route::get('params/provider', 'ParamsProviderController@get');
    Route::get('params/provider/dashboard', 'ParamsProviderController@dashboard');
    Route::post('/positif-negatif-result', 'ResultController@positifNegatif');
    Route::post('/alphanumeric-result', 'ResultController@alphanumeric');
    Route::post('/numeric-result', 'ResultController@numeric');

    Route::get('/resultat-demand/{id}', 'ResultController@getResult');

});

