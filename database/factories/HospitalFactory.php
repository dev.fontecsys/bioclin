<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Hospital::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'acronym' => $faker->word,
        'city' => $faker->city,
    ];
});
