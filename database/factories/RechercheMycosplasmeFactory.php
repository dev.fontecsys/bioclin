<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\RechercheMycosplasme::class, function (Faker $faker) {
    return [
        'prelevement' => $faker->randomElement(['1e jet urinaire', 'Sperme', 'Prélèvement urétal', 'prélèvement vaginal']),
        'culture_ureaplasma' => $faker->randomElement(['Positive', 'Negative']),
        'culture_mycoplasma' => $faker->randomElement(['Positive', 'Negative']),
        'culture_ureaplasma_titre' => $faker->word,
        'culture_mycoplasma_titre' => $faker->word,
        'culture_ureaplasma_interpretation' => $faker->randomElement(['Significatif', 'Non significatif']),
        'culture_mycoplasma_interpretation' => $faker->randomElement(['Significatif', 'Non significatif']),
        'culture_conclusion' => $faker->text,
        'culture_antibiogramme' => $faker->randomElement(['Effectué', 'Non effectué']),
        'antibiogramme_id' => factory(App\Antibiogramme::class),
    ];
});
