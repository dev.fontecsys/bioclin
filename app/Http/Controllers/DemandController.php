<?php

namespace App\Http\Controllers;

use App\Demand;
use App\Http\Requests\DemandRequest;
use App\Insurance;
use App\Patient;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class DemandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        return   Demand::with(['patient'])->orderBy("created_at",'desc')->paginate($per);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DemandRequest $request)
    {
        try
        {
            DB::beginTransaction();
            
            //on ajoute le patient dans la BDD
            if(!$request->input('patient_known'))
            {
                //Si il est inconnu de la base
                $patient = Patient::create([
                    'lastname' => $request->input('patient.lastname'),
                    'firstname' => $request->input('patient.firstname'),
                    'birthdate' => $request->input('patient.birthdate'),
                    'phone_number' => $request->input('patient.phone_number'),
                    'gender' => $request->input('patient.gender'),
                ]);
                

                    //Alors on enregistre ses assurances
                    $this->add_insurance($request->input('patient_is_insured'),$request->input('insurances'),$patient);
            }
            else
            {
                $patient = Patient::with(['insurances'])->whereId($request->input('patient.id'))->first();
                if($patient)
                {
                  if($patient->insurances()->count > 0)
                  {

                  }
                  else
                  {
                      //Si le patient n'a  aucune assurance
                      $this->add_insurance($request->input('patient_is_insured'),$request->input('insurances'),$patient); 
                  }
                }
            }

            //On recupere l'id de la dernière demande inserée
            $lastone = DB::table('demands')->latest('id')->first();
            $prefix = $lastone?$lastone->id:'1';
            $now = Carbon::now()->toDateString();

            $demand = Demand::create([
                "file_number"=>str_replace('-','',$now)."-{$prefix}",
                'patient_id' =>$patient? $patient->id : $request->input('patient.id'),
                'name_physician' => $request->input('name_physician'),
                'hospital' => $request->input('hospital'),
                'order_number_physician' => $request->input('order_number_physician'),
                'qualification_id' => $request->input('qualification_id'),
                'status' => "Enregistré",
            ]);

            //on enregistre les examens
            foreach($request->input('exams') as $exam)
            {
                $demand->tests()->attach($exam['id'],
                    [
                    "insurance_id"=>$exam['insurance_id'],
                    "price"=>$exam['current_price'],
                ]);
            }
            
            DB::commit();
            return response()->json(['success' => true,'demand'=> $demand]);
        }
        catch(\Exception $e)
        {
                DB::rollback();
                return ['status'=>false,'message'=>$e->getMessage()];

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     return  Demand::whereId($id)->with(['patient','qualification','tests','tests.examtype', 'test.resultat', 'test.exam', 'test.exam.examtype'])->orderBy("created_at",'desc')->first();
    }

    /**
     * Show the form for editing the specified resource.
     *6
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DemandRequest  $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function add_insurance($is_insured,$insurances,$patient)
    {
        //Si le nouveau patient est assuré
        if($is_insured)
        {
            //Alors on enregistre ses assurances
            foreach($insurances as $insurance)
            {
                $patient->insurances()->attach($insurance['insurance_id'],[
                    "insurance_nr"=>$insurance['insurance_number'],
                    "main_insured"=>$insurance['main_insured'],
                    "main_insured_name"=>$insurance['main_insured_name'],
                    "main_insured_nr"=>$insurance['main_insured_number'],
                ]);
            }
        }
        
    }
}
