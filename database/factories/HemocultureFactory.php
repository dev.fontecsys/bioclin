<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Hemoculture::class, function (Faker $faker) {
    return [
        'aerobie' => $faker->randomElement(['positive', ' negative']),
        'enaerobie' => $faker->randomElement(['positive', 'negative']),
        'coloration_gram' => $faker->randomElement(['diplocoque gram negatif', 'diplocoque gram positif', 'cocci gram positif', 'bacille gram negatif', 'absence de germes']),
        'culuture' => $faker->randomElement(['positive', 'négative']),
        'antibiogramme_effectue' => $faker->boolean,
        'antibiogramme_id' => factory(App\Antibiogramme::class),
    ];
});
