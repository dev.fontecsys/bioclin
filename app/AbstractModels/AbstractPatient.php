<?php
/**
 * Model object generated by: Skipper (http://www.skipper18.com)
 * Do not modify this file manually.
 */

namespace App\AbstractModels;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractPatient extends Model
{
    /**  
     * Primary key type.
     * 
     * @var string
     */
    protected $keyType = 'bigInteger';
    
    /**  
     * The attributes that should be cast to native types.
     * 
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'lastname' => 'string',
        'firstname' => 'string',
        'birthdate' => 'datetime:Y-m-d',
        'phone_number' => 'string',
        'updated_at' => 'datetime',
        'created_at' => 'datetime',
        'gender' => 'string'
    ];
    
    /**  
     * The guarded property should contain an array of attributes that you do not want to be mass assignable.
     * 
     * @var array
     */
    protected $guarded = [];
    
    public function demands()
    {
        return $this->hasMany('\App\Demand', 'patient_id', 'id');
    }
    
    public function insurances()
    {
        return $this->belongsToMany('\App\Insurance', 'insurance_to_patient', 'patient_id', 'insurance_id')->withPivot('insurance_nr', 'expiration_date','main_insured_nr','main_insured_name','main_insured');
    }
}
