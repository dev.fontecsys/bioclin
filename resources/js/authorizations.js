let user = window.App ? window.App.user : {permissions:[],role:""};

module.exports = {
    hasAbility (ability) 
    {
        return user.permissions.includes(ability)
    },
    hasAnyAbility (abilities) 
    {
      var check = false
      abilities.forEach((ability) => 
      {
        if(user.permissions.includes(ability))
        {
            check = true;
        }
      })
      return check
    },
    is(role) 
    {
        return user.role ? user.role.name ==role : false
    }
};