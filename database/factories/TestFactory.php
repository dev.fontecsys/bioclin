<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Test::class, function (Faker $faker) {
    return [
        'resultat_id' => $faker->randomNumber(),
        'resultat_type' => 8,
        'id' => $faker->randomNumber(),
        'exam_id' => $faker->numberBetween(1,81),
        'price' => $faker->numberBetween(9000,25000),
        'insurance_id' => 1,
        'demand_id' => 1,
    ];
});
