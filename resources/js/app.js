
require('./bootstrap');

window.Vue = require('vue');
require('./filter');
import vuetify from './vuetify'
import router from './router'
import store from './store/store'
import * as VeeValidate from 'vee-validate';
Vue.use(VeeValidate, { inject: false });
window.Form =  require('./classes/Form').default

// import VueEvents from 'vue-events'
// Vue.use(VueEvents)
 
// Application
import App from './components/App.vue'
import LoginComponent from './components/LoginComponent.vue'
import RegisterComponent from './components/RegisterComponent.vue'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);
Vue.component('flash',require('./components/Flash.vue').default);
Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

const app = new Vue({
    el: '#app',
    router,
    store: store,
    vuetify,
    components: {App,LoginComponent,RegisterComponent},
    data(){
        return{
            loggedIn:true
        }
    },
    created()
    {
        if(this.$store.getters['auth/isLoggedIn']){
            this.$store.dispatch('params/loadOptions')
        }
    },
    computed: {
        isLoggedIn()
        {
            
            return this.$store.getters['auth/isLoggedIn']
        },
        loggedUser()
        {
            
            return this.$store.getters['auth/_user']
        }
      },
});
