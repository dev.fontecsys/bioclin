<?php
namespace App;

class Patient extends \App\AbstractModels\AbstractPatient
{
    protected $appends=['fullName'];

    public function getFullNameAttribute()
    {
     return   $this->firstname." ".$this->lastname;
    }


}
