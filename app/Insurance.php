<?php
namespace App;

class Insurance extends \App\AbstractModels\AbstractInsurance
{
    protected $appends = ["name_upper"];

    public function getNameUpperAttribute()
    {
        return strtoupper($this->name);
    }
}
