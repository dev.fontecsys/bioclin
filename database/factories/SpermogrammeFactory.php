<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Spermogramme::class, function (Faker $faker) {
    return [
        'ph' => $faker->word,
        'ph_norme' => $faker->word,
        'volume_ejaculat' => $faker->word,
        'volume_ejaculat_unite' => $faker->word,
        'volume_ejaculat_norme' => $faker->word,
        'agglutination_spontanee' => $faker->word,
        'leucocytes' => $faker->word,
        'leucocytes_normale' => $faker->word,
        'cellules_rondes' => $faker->word,
        'cellules_rondes_unite' => $faker->word,
        'cellules_rondes_normale' => $faker->word,
        'numeration' => $faker->word,
        'numeration_normale' => $faker->word,
        'numeration_unite' => $faker->word,
        'mobilite_apres_une_heure' => $faker->word,
        'mobilite_apres_une_heure_normale' => $faker->word,
        'mobilite_apres_une_quatre_heure' => $faker->word,
        'mobilite_apres_quatre_heure_normale' => $faker->word,
        'mobilite_unite' => $faker->word,
        'vitalite' => $faker->word,
        'aspect' => $faker->word,
        'couleur' => $faker->word,
        'spermocytogramme_nb_spermatozoide_normaux' => $faker->randomNumber(),
        'spermocytogramme_nb_spermatozoide_normaux_unite' => $faker->randomNumber(),
        'spermocytogramme_anomalie_tete' => $faker->randomNumber(),
        'spermocytogramme_macrocephalie' => $faker->randomNumber(),
        'spermocytogramme_effilee' => $faker->randomNumber(),
        'spermocytogramme_ronde' => $faker->randomNumber(),
        'spermocytogramme_piece_intermediaire' => $faker->randomNumber(),
        'spermocytogramme_reste_cytoplasme' => $faker->randomNumber(),
        'spermocytogramme_grele' => $faker->randomNumber(),
        'spermocytogramme_flagelle' => $faker->randomNumber(),
        'spermocytogramme_absent' => $faker->randomNumber(),
        'spermocytogramme_ecourte' => $faker->randomNumber(),
        'spermocytogramme_enroule' => $faker->randomNumber(),
        'spermocytogramme_angulaire' => $faker->randomNumber(),
        'conclusion' => $faker->text,
        'biologiste' => $faker->word,
        'antibiogramme_id' => factory(App\Antibiogramme::class),
    ];
});
