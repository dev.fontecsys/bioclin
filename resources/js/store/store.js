import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


import members from "./modules/members"
import dashboard from "./modules/dashboard"
import params from "./modules/params"
import enterprises from "./modules/enterprises"
import examens from "./modules/examens"
import patients from "./modules/patients"
import demands from "./modules/demands"
import numerics from "./modules/numerics"
import positifnegatifs from "./modules/positifnegatifs"
import alphanumerics from "./modules/alphanumerics"

import auth from "./modules/auth"


export default  new Vuex.Store({
  modules:{
    members,
    params,
    enterprises,
    examens,
    patients,
    auth,
    dashboard,
    demands,
    numerics,
    positifnegatifs,
    alphanumerics

  },
  strict:true,
  state: {
    count: 0,
  },
  getters:{


  },
  mutations: {

  },
  computed : {
  },
  methods:
  {

  },
  actions:
  {

  }
})
