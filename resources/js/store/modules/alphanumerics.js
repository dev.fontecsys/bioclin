
const state  =
{
    paginator:{},
    initialized:false,
    items:[],
    alphanumeric:null,
    loading:false,
    loaded:false,
    alphanumericAsParameter:[],
    currentPage:0
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      initialized: state => {
        return state.initialized
      },
      alphanumericAsParameter: state => {
        return state.alphanumericAsParameter
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    alphanumeric: state => {
      return state.alphanumeric
  },
};

const actions =
{
   async getAlphanumerics({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

       await axios.get(payload.page=="" ? '/api/alphanumeric-result/' :'/api/alphanumeric-result'+payload.query).then( response =>
            {
              console.log("Promise Rejected:")

              commit('SET_ALPHANUMERICS',response.data)
              commit('SET_LOADING',false)
            })
    },
    async updateAlphanumeric({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/alphanumeric-result/'+payload.id,payload).then( response =>
            {

              commit('UPDATE_ALPHANUMERIC',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async addAlphanumeric({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.post('/api/alphanumeric-result',payload).then( response =>
            {
              // console.log(response.data)
              commit('ADD_ALPHANUMERIC',response.data.alphanumeric)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async getAlphanumeric({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.get('/api/alphanumeric-result/'+payload).then( response =>
            {
              commit('SET_ALPHANUMERIC',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async alphanumericAsParameter({commit},payload)
    {
      // commit('SET_LOADING',true)

       var result = await axios.get('/api/membres/alphanumericAsParameter?query='+payload).then( response =>
            {
              var data = Array.isArray(response.data.data) ? response.data.data : new Array()
              console.log(data)
              commit('ALPHANUMERIC_AS_PARAMETER',data)
              //commit('SET_LOADING',false)
            })
        return result;
    },
    async removeAlphanumeric({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/membres/'+payload.id).then( response =>
            {
              commit('REMOVE_ALPHANUMERIC',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },

    init({commit})
    {
            commit('SET_ALPHANUMERIC',{})
            commit('SET_ALPHANUMERICS',{})
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_ALPHANUMERICS(state,payload)
    {


        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []
            state.loaded = true
            if(state.initialized==false)
            {
            // state.initialized=true
            }
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
           state.paginator.data.forEach(element => {
             state.items.push(element)
           });
           state.loaded = true
           if(state.initialized==false)
           {
            //state.initialized=true
           }

        }


    },
    UPDATE_ALPHANUMERIC(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_ALPHANUMERIC(state,payload)
    {
      console.log(payload)
          state.items.unshift(payload)
    },
    ALPHANUMERIC_AS_PARAMETER(state,payload)
    {
          console.log("paykoad"+payload)
          state.alphanumericAsParameter = payload ? payload : new Array()
    },
    REMOVE_ALPHANUMERIC(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },

    SET_ALPHANUMERIC(state,payload)
    {
        state.alphanumeric = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    }
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
