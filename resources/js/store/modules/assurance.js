const state  =
{
    paginator:{},
    items:[],
    insurance:null,
    loading:false,
    loaded:false,
    currentPage:0
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    insurance: state => {
      return state.insurance
  },
};

const actions =
{
  async getEnterprise({commit},payload)
  {
     commit('SET_LOADING',true)

     var result = await axios.get('/api/insurances/'+payload).then( response =>
          {
            commit('SET_INSURANCE',response.data)
            commit('SET_LOADING',false)
          })
      return result;
  },
   async getEnterprises({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

       var result = await axios.get(payload.page=="" ? '/api/insurances/' :'/api/insurances'+payload.query).then( response =>
            {
              commit('SET_INSURANCES',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async updateEnterprise({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/insurances/'+payload.id,payload).then( response =>
            {

              commit('UPDATE_INSURANCE',response.data.insurance)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async addEnterprise({commit},payload)
    {

       commit('SET_LOADING',true)

       const result = await axios.post('/api/insurances',payload).then( response =>
            {
              commit('ADD_INSURANCE',response.data.insurance)
              commit('SET_LOADING',false)

            })

        return await result

    },
    async removeEnterprise({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/insurances/'+payload.id).then( response =>
            {
              commit('REMOVE_INSURANCE',payload)
              commit('SET_LOADING',false)
            })
      return result;
    },

    init({commit})
    {
            commit('SET_INSURANCES',{})
            commit('SET_ITEMS',[])
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_INSURANCES(state,payload)
    {
        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []

            state.loaded = true
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
           state.paginator.data.forEach(element => {
            state.items.push(element)
          });
          state.loaded = true

        }


    },
    UPDATE_INSURANCE(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_INSURANCE(state,payload)
    {
            state.items.unshift(payload)
    },
    REMOVE_INSURANCE(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },


    SET_ITEMS(state,payload)
    {
        state.items = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    },
    SET_INSURANCE(state,payload)
    {
        state.insurance = payload
    },
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
