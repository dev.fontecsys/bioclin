<?php

namespace App\Http\Controllers;

use App\Exam;
use Illuminate\Http\Request;
use App\Http\Requests\ExamRequest;
use Illuminate\Support\Facades\DB;

class ExamController extends Controller
{
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page"));

        return Exam::with(['examtype','insurances'])->orderBy("created_at",'desc')->paginate(500);
    }


    
    public function create()
    {
        // return view('layouts.master');
    }



    public function store(ExamRequest $request)
    {
        try
        {
            DB::beginTransaction();

            $exam = Exam::create(
            [
                "name"=>$request->input('name'),
                'base_price' =>$request->input('base_price'),
                'exam_type_id' =>$request->input('exam_type_id'),
                'result_type_id' =>$request->input('result_type_id'),
            ]);
            // dump($request->insurances);
            if($request->has('insurances')){
                foreach($request->insurances as $insurance){
                    $exam->insurances()->attach($insurance['id'],["base_price"=>$request->input('base_price'),"tm"=>$insurance['pivot']['tm'],"price"=>$insurance['pivot']['price'],"gap_tm"=>$insurance['pivot']['gap_tm'],"gap"=>$insurance['pivot']['gap']]);
                    //  $insurance['insurance_id'];
                }
            }

            DB::commit();
            return response()->json(['success' => true,'exam'=> $exam->load('insurances')]);
        }
        catch(\Exception $e)
        {
                DB::rollback();
                return ['status'=>false,'message'=>$e->getMessage()];

        }
    }



    public function show()
    {
        return view('layouts.master');
    }




    public function edit()
    {
        return view('layouts.master');
    }




    public function update(ExamRequest $request, $id)
    {
        try
        {
            DB::beginTransaction();
            $exam = Exam::findOrFail($id);
            
            $exam->name = $request->input("name");
            $exam->base_price = $request->input("base_price");
            $exam->exam_type_id = $request->input("exam_type_id");
            $exam->result_type_id = $request->input("exam_type_id");
            
            $exam->save();
            
            // dump($request->insurances);
            if($request->has('insurances')){
                $toSync = [];
                
                foreach($request->insurances as $insurance)
                {
                    $toSync[$insurance['id']]=["base_price"=>$request->input('base_price'),"tm"=>$insurance['pivot']['tm'],"price"=>$insurance['pivot']['price'],"gap_tm"=>$insurance['pivot']['gap_tm'],"gap"=>$insurance['pivot']['gap']];

                }
                $exam->insurances()->sync($toSync);

                // foreach($request->insurances as $insurance){
                //     $exam->insurances()->sync($insurance['id'],["base_price"=>$request->input('base_price'),"tm"=>$insurance['pivot']['tm'],"price"=>$insurance['pivot']['price'],"gap_tm"=>$insurance['pivot']['gap_tm'],"gap"=>$insurance['pivot']['gap']]);
                //     //  $insurance['insurance_id'];
                // }
            }

            DB::commit();
            
            return response()->json(['success' => true,'exam'=> $exam->load(['examtype','insurances'])]);
        }
        catch(\Exception $e)
        {
            dd("nnnnnn");
                DB::rollback();
                return ['status'=>false,'message'=>$e->getMessage()];

        }
    }



    public function destroy($id)
    {
        $exam = Exam::findOrFail($id)->delete();
        
        return response()->json(['message' => 'Examen supprimé avec succès'],200);
    }
}
